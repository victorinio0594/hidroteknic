﻿<?php include ('header.php') ?>


<section id="main" class="main ">

<div class="wrrape-banner" style="background-image: url(images/contacto.jpg)">
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-end">
            <div class="col-12 col-lg-6 text-left">
                <div class="blanco font-36 bebas700 text-uppercase">PRODUCTOS</div>
            </div>
            <div class="col-12 col-lg-6 d-none d-lg-block">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb p-0 mb-0 bg-transparent justify-content-end">
                        <li class="breadcrumb-item"><a href="#" class="bar200 blanco font-18">Home </a></li>
                        <li class="breadcrumb-item font-18 bar200 blanco active" aria-current="page">Productos </li>
                        <li class="breadcrumb-item font-18 bar200 blanco active" aria-current="page">Hidrocool G30</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class=" content_contacto">

    <div class="bg-blanco py-5">
        <div class="container ">
            <div class="row ">
                <div class="col-12 col-lg-10">
                    
                    <div class="row filtros">
                        
                        <div class="input-group col-md-4 col-12 mb-3 mb-lg-0">
                            <input type="text" class="form-control" placeholder="Buscar..." aria-label="Buscar..." aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-primary p-0 bg-azul px-3" type="button" id="button-addon2"><img src="images/search.png" alt=""></button>
                            </div>
                        </div>

                        <div class="input-group col-md-4 col-12 mb-3 mb-lg-0">
                            <select class="custom-select" id="inputGroupSelect01">
                                <option selected>Precio...</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>

                        <div class="input-group col-md-4 col-12 mb-3 mb-lg-0">
                            <select class="custom-select" id="inputGroupSelect01">
                                <option selected>Tipo...</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>


                    </div>
                </div>
                <div class="col-12 col-lg-2">
                    <a href="" class="btn btn-outline-primary font-14 p-1 px-3">FILTRAR </a>
                </div>
            </div>
        </div>
    </div>

    <div class="container py-5">
        
        <div class="row">
            <div class="col-12 col-md-6 mb-4 col-lg-3">
                <div class="card bg-blanco text-center">
                    <div class="card-body">
                        <img src="images/producto.png" alt="">
                        <div class="card-title mb-1 azul40 font-24 bebas700">nombre del producto</div>
                        <div class="card-text bra400 font-16 negro mb-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, </div>
                        <a href="#" class="btn btn-outline-primary">Ver más</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 mb-4 col-lg-3">
                <div class="card bg-blanco text-center">
                    <div class="card-body">
                        <img src="images/producto.png" alt="">
                        <div class="card-title mb-1 azul40 font-24 bebas700">nombre del producto</div>
                        <div class="card-text bra400 font-16 negro mb-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, </div>
                        <a href="#" class="btn btn-outline-primary">Ver más</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 mb-4 col-lg-3">
                <div class="card bg-blanco text-center">
                    <div class="card-body">
                        <img src="images/producto.png" alt="">
                        <div class="card-title mb-1 azul40 font-24 bebas700">nombre del producto</div>
                        <div class="card-text bra400 font-16 negro mb-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, </div>
                        <a href="#" class="btn btn-outline-primary">Ver más</a>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 mb-4 col-lg-3">
                <div class="card bg-blanco text-center">
                    <div class="card-body">
                        <img src="images/producto.png" alt="">
                        <div class="card-title mb-1 azul40 font-24 bebas700">nombre del producto</div>
                        <div class="card-text bra400 font-16 negro mb-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, </div>
                        <a href="#" class="btn btn-outline-primary">Ver más</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</section>


<?php include ('footer.php') ?>