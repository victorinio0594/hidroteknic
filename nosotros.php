﻿<?php include ('header.php') ?>


<section id="main" class="main ">

<div class="wrrape-banner" style="background-image: url(images/contacto.jpg)">
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-end">
            <div class="col-12 col-lg-6 text-left">
                <div class="blanco bebas700 font-36 text-uppercase">QUIENES SOMOS</div>
            </div>
            <div class="col-12 col-lg-6 d-none d-lg-block">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb p-0 mb-0 bg-transparent justify-content-end">
                        <li class="breadcrumb-item"><a href="#" class="blanco bar200 font-18">Home</a></li>
                        <li class="breadcrumb-item bar200 font-18 blanco active" aria-current="page">Hidrotecnik</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="content_nosotros">
    <div class="container py-5">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-lg-6 pr-lg-5 pb-5 pb-lg-0">
                <img src="images/hidrotecnik-web.png" class="w-100" alt="">
            </div>
            <div class="col-12 col-lg-6">
                <div class="font-24 azul40 bar700 text-uppercase pb-3 text-left">HIDROTECNIK S.A.S. Proveedor Colombiano, líder en ingeniería y tecnologías de Aguas.</div>
                <div class="gris bar400 font-16  "> 
                    HIDROTECNIK S.A.S. Proveedor Colombiano, líder en ingeniería y tecnologías de Aguas. HIDROTECNIK S.A.S Proveedor Colombiano, líder en ingeniería y tecnologías de Aguas. Somos una empresa de ingeniería enfocada a la protección del Medio Ambiente, especializados en control de emisiones y equipos para tratamiento de aguas. Hidrotecnik es el primer productor certificado en Colombia en la producción de Agente Reductor de Emisiones AUS 32 DEF (Diesel Exaust Fluid). Producción local con cumplimiento de los mejores estándares internacionales. Liderazgo y orientación al servicio y soporte de las necesidades de nuestros clientes.
                </div>
            </div>           
        </div>     
             
    </div>
    
</div>

<div  class="content_showus position-relative py-5">
    <div id="content_showus"></div>     
    <div class="container">
        <div class="font-32 negro bebas700 text-uppercase mb-2 text-left">¿POR QUÉ ESCOGERNOS?  </div>
        <div class="font-24 azul40 bebas700 text-uppercase pb-3 text-left">Excelente portafolio de productos con valor agregado y alto impacto ambiental.</div>

        <div class="row">
            <div class="col-12 col-lg-6 mb-5 mb-lg-0">
                <div class="accordion mt-3" id="accordionExample">
                    <div class="card">
                        <div class="card-header" id="headingOne">                            
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                hidrotecnik
                            </button>                            
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            Hidrotecnik, es el primer productor certificado en Colombia, para la venta de Urea Automotriz, para el consumo de las flotas, sistemas de transporte masivo y motores diesel con motores Euro 4, 5 y 6.
                        </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingTwo">                        
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                ventas
                            </button>                     
                        </div>
                        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                            <div class="card-body">
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingThree">                       
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                proveedores
                            </button>                        
                        </div>
                        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                            <div class="card-body">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <img src="images/nosotros-web.png" alt="" class="w-100">
            </div>
        </div>
    </div>
</div>


<div class="content_reconocimientos py-5">
    <div class="container">    

        <ul class="nav nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">CERTIFICACIONES</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">RECONOCIMIENTOS</a>
            </li>
        </ul>

        <div class="tab-content py-4" id="pills-certificados">
            <div class="tab-pane w-100 fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">

                <div class="owl-carousel owl-theme owl-certificados ">
                    <div class="item"><a href=""> <img src="images/certificados/Certifica-1.jpg " alt=""></a></div>
                    <div class="item"><a href=""> <img src="images/certificados/Certifica-2.jpg " alt=""></a></div>
                    <div class="item"><a href=""> <img src="images/certificados/Certifica-3.jpg " alt=""></a></div>
                    <div class="item"><a href=""> <img src="images/certificados/Certifica-4.jpg " alt=""></a></div>
                </div>          
           
            </div>
            <div class="tab-pane  w-100 fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                <div class="owl-carousel owl-theme owl-certificados ">
                    <div class="item"><a href=""> <img src="images/certificados/01.png" alt=""></a></div>
                    <div class="item"><a href=""> <img src="images/certificados/02.png" alt=""></a></div>
                </div>                  
            </div>
        </div>

    </div>
</div>



</section>


<?php include ('footer.php') ?>