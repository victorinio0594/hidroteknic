﻿<?php include ('header.php') ?>


<section id="main" class="main pt-5">

    <div class="owl-carousel owl-theme owl-home pt-lg-4">
        <div class="item"><img src="images/banner-01.jpg" alt=""></div>
        <div class="item"><img src="images/banner-01.jpg" alt=""></div>
    </div>

    <div class="homeProductos">
        <div class="row mx-0">
            <div class="col-12 col-lg-6 bg-homeproductos">
                <div class="row mx-0 justify-content-center align-items-center py-4 px-sm-4">
                    <div class="col-12 col-sm-6">
                        <div class="negro font-32 bebas700 text-uppercase mb-3">urea automotriz </div>
                        <img src="images/producto01.png" class="d-block" alt="">
                        <a href="productos.php" class="btn btn-outline-primary  "> VER MÁS </a>                    
                    </div>
                    <div class="col-12 col-sm-6">
                        <img src="images/producto001.png" class="w-100 max-10" alt="">
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6 bg-homeproductos">
                <div class="row mx-0 justify-content-center align-items-center py-4 px-sm-4">
                    <div class="col-12 col-sm-6">
                        <div class="negro font-32 bebas700 text-uppercase mb-3">refrigerantes</div>
                        <img src="images/producto02.png" class="d-block" alt="">
                        <a href="productos.php" class="btn btn-outline-primary  "> VER MÁS </a>                    
                    </div>
                    <div class="col-12 col-sm-6">
                        <img src="images/producto002.png"  class="w-100 max-10" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="homeForm bg-azul20">
        <div class="container py-5">
            <div class="row justify-content-center align-items-center">
                <div class="col-12 d-none d-lg-block col-lg-7 mb-4 mb-lg-0">
                    <img src="images/homeCart.png" class="w-100 pr-lg-5" alt="">
                </div>
                <div class="col-12 col-lg-5">
                    <div class="font-32 bebas700 mb-4 text-uppercase blanco">contáctanos</div>
                    <form class=""  method="post">                        
                        <div class="form-group ">
                            <label class="form-label" for="input1">Nombre*</label>
                            <input type="text" id="input1" class="form-control"  />
                        </div>
                        <div class="form-group ">
                            <label class="form-label" for="input2">Teléfono*</label>
                            <input type="text" id="input2" class="form-control"  />
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="input3">Email*</label>
                            <input type="text" id="input3" class="form-control"  />
                        </div>
                        <div class="form-group ">
                            <label class="form-label" for="input6">Mensaje**</label>
                            <textarea name="" id="input6" class="form-control"></textarea>                            
                        </div>
                        
                        <div class="form-group">
                            <div class="form-check text-center">
                            <input class="form-check-input" type="checkbox" id="gridCheck">
                               <label class="form-check-label" for="gridCheck">
                                <a href="terminos-condiciones.php" class="blanco" target="_blank">  Acepto términos y condiciones</a>
                                </label>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="button" class="btn btn-light">ENVIAR </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="homeSomos d-flex justify-content-center align-items-center py-5">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class="font-32 bebas700 mb-4 text-uppercase negro">somos hidrotecnik</div>
                    <div class="font-24 bar400 azul40 mb-5">Excelente portafolio de productos con <br> valor agregado y alto impacto ambiental.</div>
                    <a href="nosotros.php#content_showus" class="btn btn-outline-primary">VER MÁS </a>
                </div>
            </div>
        </div>
    </div>


    <div class="homeCobertura">
        <div class="container ">
            <div class="row justify-content-center align-items-center">
                <div class="col-12 col-lg-6 pt-lg-4 order-2 order-lg-1">
                    <img src="images/cobertura.png" class="w-100" alt="">
                </div>
                <div class="col-12 col-lg-6 px-lg-5 order-1 order-lg-2 py-5 py-lg-0">
                    <div class="font-32 bebas700 mb-2 text-uppercase negro">cobertura</div>
                    <div class="font-24 bar400 azul40 mb-3">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.</div>
                    <div class="font-18 bar400 negro text-hidden"> Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. </div>
                </div>
            </div>
        </div>
    </div>

    <div class="homeLastcontent ">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-lg-6 pt-4 mb-5 mb-lg-0">
                    <div class="font-32 bebas700 mb-3 text-uppercase negro">Felicitaciones</div>
                    <div class="owl-carousel owl-theme owl-testimonios px-4 px-lg-5">
                        <div class="item">
                            <div class="media">
                            <img src="images/users.png" class="align-self-start mr-3 user-testimonio" alt="...">
                                <div class="media-body font-18 bar400 negro ">
                                    <div class="text-hidden" >Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</div>
                                    <div class="mt-4 mb-0 font-16 bar700 negro">- Merry Jain</div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="media">
                            <img src="images/users.png" class="align-self-start mr-3 user-testimonio" alt="...">
                                <div class="media-body font-18 bar400 negro text-hidden">
                                    <div class="text-hidden" >Donec sed odio dui. Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</div>
                                    <div class="mt-4 mb-0 font-16 bar700 negro">- Merry Jain</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 homePqrs">
                    <div class="w-50 float-right py-5">
                        <div class="font-24 bar400 azul40 titulo-hidden mb-2">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy.</div>
                        <div class="font-18 bar400 negro text-hidden"> Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi. </div>
                        <div class="text-center mt-4">
                            <a href="pqrs.php" class="btn btn-outline-primary "> PQRS </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="owl-carousel owl-theme owl-redes ">
        <div class="item"><img src="images/redes.jpg" alt=""></div>
        <div class="item"><img src="images/redes.jpg" alt=""></div>
        <div class="item"><img src="images/redes.jpg" alt=""></div>
        <div class="item"><img src="images/redes.jpg" alt=""></div>
        <div class="item"><img src="images/redes.jpg" alt=""></div>
        <div class="item"><img src="images/redes.jpg" alt=""></div>
        <div class="item"><img src="images/redes.jpg" alt=""></div>
        <div class="item"><img src="images/redes.jpg" alt=""></div>
        <div class="item"><img src="images/redes.jpg" alt=""></div>
        <div class="item"><img src="images/redes.jpg" alt=""></div>
        <div class="item"><img src="images/redes.jpg" alt=""></div>
    </div>


</section>


<?php include ('footer.php') ?>