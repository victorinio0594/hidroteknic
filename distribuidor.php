﻿<?php include ('header.php') ?>


<section id="main" class="main ">

<div class="wrrape-banner" style="background-image: url(images/contacto.jpg)">
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-end">
            <div class="col-12 col-lg-6 text-left">
                <div class="blanco bebas700 font-36 text-uppercase">SER DISTRIBUIDOR</div>
            </div>
            <div class="col-12 col-lg-6 d-none d-lg-block">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb p-0 mb-0 bg-transparent justify-content-end">
                        <li class="breadcrumb-item"><a href="#" class="blanco bar200 font-18">Home</a></li>
                        <li class="breadcrumb-item font-18 blanco bar200 active" aria-current="page">Distribuidor</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="content_contacto  py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-5 order-2 order-md-1">
                <div class="content-form">
                    <form class=""  method="post">                        
                        <div class="form-group ">
                            <label class="form-label" for="input1">Nombre*</label>
                            <input type="text" id="input1" class="form-control"  />
                        </div>
                        <div class="form-group ">
                            <label class="form-label" for="input2">Teléfono*</label>
                            <input type="text" id="input2" class="form-control"  />
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="input3">Email*</label>
                            <input type="text" id="input3" class="form-control"  />
                        </div>
                        <div class="form-group ">
                            <label class="form-label" for="input4">País*</label>
                            <input type="text" id="input4" class="form-control"  />
                        </div>
                        <div class="form-group ">
                            <label class="form-label" for="input5">Asunto*</label>
                            <input type="text" id="input5" class="form-control"  />
                        </div>
                        <div class="form-group ">
                            <label class="form-label" for="input6">Mensaje**</label>
                            <textarea name="" id="input6" class="form-control"></textarea>                            
                        </div>
                        
                        <div class="form-group">
                            <div class="form-check text-center">
                            <input class="form-check-input" type="checkbox" id="gridCheck">
                               <label class="form-check-label" for="gridCheck">
                                <a href="terminos-condiciones.php" class="negro" target="_blank">  Acepto términos y condiciones</a>
                                </label>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="button" class="btn btn-outline-primary  ">ENVIAR </button>
                        </div>
                    </form>
                </div>
            </div> 
            <div class="col-12 col-md-6 col-lg-7 mb-5 mb-lg-0  order-1 order-md-2">
                <div class="gris mb-3 font-16 bar400"> 
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Repellat, quidem labore! Officiis a consectetur, nisi iure, aliquid cumque adipisci officia eaque at in tempora qui. Ad sed blanditiis molestias voluptas.
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Iure facere aspernatur dolore illo dignissimos quia, odio laboriosam! Pariatur eligendi, velit provident quas omnis id itaque quia repudiandae non minima natus.
                </div>
                <div class="negro font-32 bebas700 text-uppercase mb-3">BENEFÍCIOS </div>

                <div class="row items_beneficios">
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3 mb-4 mb-lg-0 text-center">
                            <img src="images/icon1.jpg" clasS="mb-2 w-100" alt="">
                            <div class="azul40 font-18 bebas400 ">Trayectoria </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3 mb-4 mb-lg-0 text-center">
                            <img src="images/icon2.jpg" clasS="mb-2 w-100" alt="">
                            <div class="azul40 font-18 bebas400 ">Certificaciones </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3 mb-3 mb-lg-0 text-center">
                            <img src="images/icon3.jpg" clasS="mb-2 w-100" alt="">
                            <div class="azul40 font-18 bebas400 ">Calidad </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-6 col-lg-3 mb-3 mb-lg-0 text-center">
                            <img src="images/icon4.jpg" clasS="mb-2 w-100" alt="">
                            <div class="azul40 font-18 bebas400 ">Servicio </div>
                    </div>
                </div>

                <div class="owl-carousel mt-5 multimedia owl-distribuidor owl-theme px-lg-4">
                    <div class="item ">
                        <a href="images/redes.jpg">
                            <img src="images/redes.jpg">   
                        </a>       
                    </div>
                    <div class="item ">
                        <a href="images/redes.jpg">
                            <img src="images/redes.jpg">   
                        </a>           
                    </div>
                    <div class="item ">                         
                        <a href="images/redes.jpg">
                            <img src="images/redes.jpg">   
                        </a>                         
                    </div>
                    <div class="item ">                         
                        <a href="images/redes.jpg">
                            <img src="images/redes.jpg">   
                        </a>                          
                    </div>
                </div> 

            </div>
        </div>
       
    </div>
</div>

</section>


<?php include ('footer.php') ?>