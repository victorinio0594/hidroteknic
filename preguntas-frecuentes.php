﻿<?php include ('header.php') ?>


<section id="main" class="main ">

<div class="wrrape-banner" style="background-image: url(images/contacto.jpg)">
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-end">
            <div class="col-12 col-lg-6 text-left">
                <div class="blanco bebas700 font-36 text-uppercase">preguntas frecuentes</div>
            </div>
            <div class="col-12 col-lg-6 d-none d-lg-block">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb p-0 mb-0 bg-transparent justify-content-end">
                        <li class="breadcrumb-item"><a href="#" class="blanco bar200 font-18">Home</a></li>
                        <li class="breadcrumb-item bar200 font-18 blanco active" aria-current="page">Hidrotecnik</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>


<div class="content_showus  py-5 position-relative">
    <div class="content_preguntas content_pqrs h-100">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-lg-7 mb-5 mb-lg-0">
                    <div class="accordion mt-3" id="preguntas_frecuentes">
                        <div class="card">
                            <div class="card-header" id="headingOne">                            
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    hidrotecnik
                                </button>                            
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#preguntas_frecuentes">
                            <div class="card-body">
                                Hidrotecnik, es el primer productor certificado en Colombia, para la venta de Urea Automotriz, para el consumo de las flotas, sistemas de transporte masivo y motores diesel con motores Euro 4, 5 y 6.
                            </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">                        
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    ventas
                                </button>                     
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#preguntas_frecuentes">
                                <div class="card-body">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nesciunt nobis culpa, laborum aliquid et, tempore aspernatur, eligendi error debitis voluptatibus mollitia tenetur. Quisquam aliquid ut maiores aperiam officiis repudiandae quae?
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">                       
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    proveedores
                                </button>                        
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#preguntas_frecuentes">
                                <div class="card-body">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nesciunt nobis culpa, laborum aliquid et, tempore aspernatur, eligendi error debitis voluptatibus mollitia tenetur. Quisquam aliquid ut maiores aperiam officiis repudiandae quae?
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingFour">                       
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    proveedores II
                                </button>                        
                            </div>
                            <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#preguntas_frecuentes">
                                <div class="card-body">
                                Lorem ipsum, dolor sit amet consectetur adipisicing elit. Nesciunt nobis culpa, laborum aliquid et, tempore aspernatur, eligendi error debitis voluptatibus mollitia tenetur. Quisquam aliquid ut maiores aperiam officiis repudiandae quae?
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>



</section>


<?php include ('footer.php') ?>