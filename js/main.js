$(document).ready(function() {


	$('.owl-certificados').owlCarousel({
		loop:false,
		margin:0,
		nav: false,
		dots: false,
		autoplay: false,		
		smartSpeed: 650,
		mouseDrag: true,
		autoplayHoverPause: true,
		autoplayTimeout: 2000,
		responsiveClass:true,
		autoHeight: false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:4
			},
			1000:{
				items:6
			}
		}
	});

	$('.owl-distribuidor').owlCarousel({
		loop:false,
		margin:10,
		nav: true,
		dots: false,
		autoplay: false,		
		navText: ["<img src='images/test-left.png'>","<img src='images/test-right.png'>"],	
		smartSpeed: 650,
		mouseDrag: true,
		autoplayHoverPause: true,
		autoplayTimeout: 2000,
		responsiveClass:true,
		autoHeight: false,
		responsive:{
			0:{
				items:1,
				nav: false,
				dots: true,
			},
			600:{
				items:2,
				nav: false,
				dots: true,		
			},
			1000:{
				items:3,
				nav: false,
				dots: true,
			}
		}
	});
	
	$('.owl-productos').owlCarousel({
		loop:true,
		margin:0,
		nav: false,
		dots: false,
		autoplay: false,		
		smartSpeed: 650,
		mouseDrag: true,
		autoplayHoverPause: true,
		autoplayTimeout: 2000,
		responsiveClass:true,
		autoHeight: false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:3
			}
		}
	});

	$('.owl-testimonios').owlCarousel({
		loop:true,
		margin:0,
		nav: true,
		dots: false,
		autoplay: true,	
		navText: ["<img src='images/test-left.png'>","<img src='images/test-right.png'>"],	
		smartSpeed: 650,
		mouseDrag: true,
		autoplayHoverPause: true,
		autoplayTimeout: 6000,
		responsiveClass:true,
		autoHeight: false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:1
			},
			1000:{
				items:1
			}
		}
	}); 

	$('.owl-menu').owlCarousel({
		loop:true,
		margin:0,
		nav: false,
		dots: false,
		autoplay: true,		
		smartSpeed: 650,
		mouseDrag: true,
		autoplayHoverPause: true,
		autoplayTimeout: 6000,
		responsiveClass:true,
		autoHeight: false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			1000:{
				items:3
			}
		}
	}); 

	$('.owl-redes').owlCarousel({
		loop:true,
		margin:0,
		nav: false,
		dots: false,
		autoplay: true,		
		smartSpeed: 650,
		mouseDrag: true,
		autoplayHoverPause: true,
		autoplayTimeout: 2000,
		responsiveClass:true,
		autoHeight: false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:6
			},
			1000:{
				items:8
			}
		}
	});

	$('.owl-home').owlCarousel({
		loop:true,
		margin:0,
		nav: false,
		dots: true,
		autoplay: true,		
		smartSpeed: 650,
		mouseDrag: true,
		autoplayHoverPause: true,
		autoplayTimeout: 4000,
		responsiveClass:true,
		autoHeight: false,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:1
			},
			1000:{
				items:1
			}
		}
	});

	$('.search-button').click(function(){
		$(this).parent().toggleClass('open');
	});

	$('.form-control').focus(function(){
		$(this).parents('.form-group').addClass('focused');
		});

		$('.form-control').blur(function(){
		var inputValue = $(this).val();
		if ( inputValue == "" ) {
			$(this).removeClass('filled');
			$(this).parents('.form-group').removeClass('focused');  
		} else {
			$(this).addClass('filled');
		}
	});	

	$('#Gallery').lightSlider({
        gallery:true,
        item:1,
        loop:true,
		// thumbItem:9,
		slideMove:1,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed:600,
        slideMargin:0,
		enableDrag: false,
		responsive : [
            {
                breakpoint:800,
                settings: {
                    item:1,
                    slideMove:1,
                    slideMargin:6,
                  }
            },
            {
                breakpoint:480,
                settings: {
                    item:1,
                    slideMove:1
                  }
            }
        ],
        currentPagerPosition:'left',
        onSliderLoad: function(el) {
            el.lightGallery({
                selector: '#imageGallery .lslide'
            });
        }   
    });  

	$('.multimedia .item').lightGallery({
		animateThumb: true,
		showThumbByDefault: false
	 });


}); //fin de la funcion ready

function mobileMenuOpen() {
	document.getElementById("dropdownMenuLink").classList.toggle("show");
}