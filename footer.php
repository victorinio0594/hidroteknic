<?php if ($vista == 'micro') {  ?>
       
<?php } else { ?>    
    <footer class="pb-5">
        <div class="container">
            <div class="row mx-0 mx-lg-auto pt-5 bd-top justify-content-center align-items-center align-items-lg-start">
                <div class="col-12 col-md-12 col-lg-4 mb-5 mb-lg-0  text-left">
                    <img src="images/logo.png" alt="" class="logoFooter mb-5">
                    <div class="blanco font-18 bar400 pr-4 mb-4 text-left">
                        Somos una empresa de ingeniería, enfocada en la protección del Medio Ambiente, especializada en el control de emisiones y equipos para tratamiento de aguas. Primer fabricante certificado en Colombia de Urea Automotriz
                    </div>
                    <ul class="mb-0 list-unstyled">
                        <li> 
                            <a href="https://www.facebook.com/profile.php?id=100009586727371 "><img src="images/facebook.png" alt=""></a>
                            <a href="https://www.instagram.com/hidrotecnik.sas/?hl=es-la" ><img src="images/instagram.png" alt=""></a>
                            <a href="https://www.linkedin.com/company/7091449/" target="_blank"><img src="images/linkedin.png" alt=""></a>
                        </li>
                    </ul>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 mb-5 mb-sm-0 pl-sm-0">
                    <div class="row mx-0 justify-content-end text-left">
                        <div class="col-12 col-sm-6 col-md-6 pl-sm-0  mb-5 mb-lg-0">
                            <img src="images/logoFotter2.png" alt="" class="mb-5 logosFooter">
                            <div class="blanco text-uppercase font-22 bar700 mb-4">PRODUCTOS</div>
                            <a href="single-producto.php" class="blanco bar500 font-18 d-block mb-2 ml-sm-2">HidroCool</a>
                            <a href="single-producto.php" class="blanco bar500 font-18 d-block mb-2 ml-sm-2">HidroBlue1</a>
                        </div>
                        <div class="col-12 col-sm-6 col-md-6 pl-sm-0 pr-sm-0 ">
                            <img src="images/ntc.png" alt="" class="mb-5 logosFooter">
                            <div class="blanco text-uppercase font-22 bar700 mb-4">información</div>
                            <a href="nosotros.php" class="blanco bar500 font-18 d-block mb-2 ml-sm-2">Sobre nosotros</a>
                            <a href="nosotros.php" class="blanco bar500 font-18 d-block mb-2 ml-sm-2">Certificaciones</a>     
                            <a href="distribuidor.php" class="blanco bar500 font-18 d-block mb-2 ml-sm-2">Ser distribuidor</a>                         
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-4">
                    <div class="blanco text-uppercase bar700 font-22 mb-4">CONTACTO</div>
                    <div class="blanco mb-4 bar400 font-18">
                        <strong class="d-block bar700 mb-0"> Dirección</strong>                                                
                        Parque Industrial San Miguel Bodega 1a
                        km 1.5 via siberia-cota Entrada potrero chico
                        Cota, cundinamarca, Colombia.
                    </div>
                    <div class="blanco mb-4 bar400 font-18">
                        <strong class="d-block bar700 mb-0"> Email</strong>      
                        soporteyventas@hidrotecnik.com <br>
                        ventas1@hidrotecnik.com
                    </div>
                    <div class="blanco mb-2 bar400 font-18">
                        <strong class="d-block bar700 mb-0"> Línea de atenció al cliente</strong>   (+571) 8985017 • (+571) 8985027
                    </div>
                    <div class="blanco mb-2 bar400 font-18">
                        <strong class="d-block bar700 mb-0"> Celular ventas</strong> (+57) 321 201 48 37 • (+57) 317 278 80 75
                    </div>
                </div>
            </div>
        </div>
    </footer>
<?php } ?>

<script src="js/base/jquery.min.js"></script>
<script src="js/base/bootstrap.min.js"></script> 
<script src="js/base/all.min.js"></script>
<script src="js/base/animatescroll.js"></script>
<script src="js/base/owl.carousel.min.js"></script>
<script src="js/base/lightgallery.min.js"></script>
<script src="js/base/lg-thumbnail.min.js"></script>
<script src="js/base/lg-video.min.js"></script>
<script src="js/base/lightslider.min.js"></script>

<script src="js/main.js"></script>


</body>
</html>