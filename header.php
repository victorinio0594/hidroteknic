<?php error_reporting(0); ?>
<!doctype html>
<html lang="es">
<head>
<meta charset="utf-8">
<title> </title>
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta name="description" content="" />
<meta name="keywords" content="" />
<meta name="author" content="" />

<!-- favicon links -->
<link rel="shortcut icon" type="image/ico" href="images/favicon.png" />
<link rel="icon" type="image/ico" href="images/favicon.png"/>
<!-- main css -->

<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/fontawesome.min.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/lightgallery.min.css"/>
<link rel="stylesheet" href="css/lightslider.css">
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">

<link rel="stylesheet" href="css/main.css">


</head>

<body>

<?php if ($vista == 'galeria') {  ?>
       
<?php } else { ?>

<header>
	
	<nav id="navbar" class="navbar flex-column fixed-top navbar-expand-lg navbar-default ">
		<div class="menu_top w-100 pb-1">
			<div class="p-1">
				<div class="row mx-0 mt-lg-1 mb-lg-n1 justify-content-center align-items-center ">
					<div class="col-8 col-sm-6 col-lg-5 col-xl-4  pl-0 pl-lg-2 mb-sm-0">
						<div class="row align-items-center mx-0">
							<div class="col-6 col-sm-6 pl-0 pl-sm-4">
								<a class="navbar-brand mr-0"  href="index.php"><img src="images/logo.png" width="100%" alt=""></a>
							</div>
							<div class="col-6 col-sm-5 text-center text-xl-left">
								<a href=""><img src="images/ntc.png" class="imgNtc"  width="100%" alt=""></a>
							</div>
						</div>
					</div>
					<div class="col-2 col-sm-6 col-lg-7 col-xl-6 mb-sm-0 text-right">
						<div class="d-none d-lg-flex justify-content-between align-items-center">
							<a href="" class="blanco font-14 d-flex justify-content-center align-items-center bar400 "><img src="images/tele.png" class="imgHeader mr-1" alt=""> soporteyventas@hidrotecnik.com</a>
							<a href="" class="blanco font-14 d-flex justify-content-center align-items-center bar400 "><img src="images/mail.png" class="imgHeader mr-1" alt=""> (+57) 321 201 48 37 • (+57) 317 278 80 75</a>
						</div>

						<button class="navbar-toggler blanco " type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
							<i class="fas fa-bars"></i>
						</button>
					</div>
					<div class="col-12 col-sm-4 col-lg-2 col-xl-2 d-none d-xl-inline text-right">
						<ul class="mb-0 list-unstyled">
							<li> 
								<a href="https://www.facebook.com/profile.php?id=100009586727371" target="_blank"><img src="images/facebook.png" class="imgHeader" alt=""></a>
								<a href="https://www.instagram.com/hidrotecnik.sas/?hl=es-la" target="_blank"><img src="images/instagram.png"  class="imgHeader" alt=""></a>
								<a href="https://www.linkedin.com/company/7091449/" target="_blank"><img src="images/linkedin.png" class="imgHeader" alt=""></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>	
		
		<div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
			<ul class=" nav align-items-center flex-column flex-lg-row ">
				<li class="nav-item active">
					<a class="nav-link bebas font-18" href="index.php" >HOME <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link bebas font-18" href="nosotros.php">HIDROTECNIK</a>
				</li>
				<li class="nav-item">
					<div class="dropdown p-0">
						<button  class="nav-link bebas font-18 dropdown-toggle" onclick="mobileMenuOpen()" > PRODUCTOS </button>

						<div class="dropdown-menu p-0" id="dropdownMenuLink">
							<div class="row mx-0">
								<div class="col-4 px-0">
									<div class="nav flex-column nav-pills" id="v-headers-tab" role="tablist" aria-orientation="vertical">
										<a class="nav-link active" id="v-headers-home-tab" data-toggle="pill" href="#v-headers-home" role="tab" aria-controls="v-headers-home" aria-selected="true">UREA</a>
										<a class="nav-link" id="v-headers-profile-tab" data-toggle="pill" href="#v-headers-profile" role="tab" aria-controls="v-headers-profile" aria-selected="false">REFRIGERANTES</a>
										<a class="nav-link" id="v-headers-messages-tab" data-toggle="pill" href="#v-headers-messages" role="tab" aria-controls="v-headers-messages" aria-selected="false">PRODUCTO</a>
										<a class="nav-link" href="productos.php" >Todos los productos</a>

									</div>
								</div>
								<div class="col-8 px-0">
									<div class="tab-content" id="v-headers-tabContent">
										<div class="tab-pane fade delay-1s show active" id="v-headers-home" role="tabpanel" aria-labelledby="v-headers-home-tab">
											<div class="owl-carousel owl-theme owl-menu">
												<div class="item"> <a href="single-producto.php" class="p-2 negro bar400 text-center font-14"> <img src="images/producto.png" alt="">  HIDROCOOL G48 </a> </div>
												<div class="item"> <a class="p-2 negro bar400 text-center font-14" href="single-producto.php"> <img src="images/producto.png" alt="">  HIDROCOOL G48 </a></div>
												<div class="item"> <a class="p-2 negro bar400 text-center font-14" href="single-producto.php"> <img src="images/producto.png" alt="">  HIDROCOOL G48 </a></div>
											</div>
										</div>
										<div class="tab-pane fade delay-1s" id="v-headers-profile" role="tabpanel" aria-labelledby="v-headers-profile-tab">
											<div class="owl-carousel owl-theme owl-menu">
												<div class="item"> <a class="p-2 negro bar400 text-center font-14" href="single-producto.php"><img src="images/producto.png" alt="">  HIDROCOOL G48 </a> </div>
												<div class="item"> <a class="p-2 negro bar400 text-center font-14" href="single-producto.php"> <img src="images/producto.png" alt="">  HIDROCOOL G48 </a></div>
												<div class="item"> <a class="p-2 negro bar400 text-center font-14" href="single-producto.php"> <img src="images/producto.png" alt="">  HIDROCOOL G48 </a></div>
											</div>
										</div>
										<div class="tab-pane fade delay-1s" id="v-headers-messages" role="tabpanel" aria-labelledby="v-headers-messages-tab">
											<div class="owl-carousel owl-theme owl-menu">
												<div class="item"> <a class="p-2 negro bar400 text-center font-14" href="single-producto.php"> <img src="images/producto.png" alt="">  HIDROCOOL G48 </a> </div>
												<div class="item"> <a class="p-2 negro bar400 text-center font-14" href="single-producto.php"> <img src="images/producto.png" alt="">  HIDROCOOL G48 </a></div>
												<div class="item"> <a class="p-2 negro bar400 text-center font-14" href="single-producto.php"> <img src="images/producto.png" alt="">  HIDROCOOL G48 </a></div>
											</div>
										</div>
										
									</div>
								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link bebas font-18" href="preguntas-frecuentes.php">PREGUNTAS FRECUENTES</a>
				</li>
				<li class="nav-item">
					<a class="nav-link bebas font-18" href="contacto.php">CONTACTO</a>
				</li>   
				<li class="nav-item ml-lg-1">
					<div class="search">
						<input type="search" class="search-box" />
						<span class="search-button">
							<span class="search-icon"></span>
						</span>
					</div>
				</li> 				   
			</ul>
		</div>
	</nav>
</header>

<?php } ?>