﻿<?php include ('header.php') ?>


<section id="main" class="main ">

<div class="wrrape-banner" style="background-image: url(images/contacto.jpg)">
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-end">
            <div class="col-12 col-lg-6 text-left">
                <div class="blanco font-36 bebas700 text-uppercase">PRODUCTOS</div>
            </div>
            <div class="col-12 col-lg-6 d-none d-lg-block">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb p-0 mb-0 bg-transparent justify-content-end">
                        <li class="breadcrumb-item"><a href="#" class="bar200 blanco font-18">Home </a></li>
                        <li class="breadcrumb-item font-18 bar200 blanco active" aria-current="page">Productos </li>
                        <li class="breadcrumb-item font-18 bar200 blanco active" aria-current="page">Hidrocool G30</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="content_nosotros">
    <div class="container py-5">
       
        <div class="row justify-content-center align-items-center mb-4">
            <div class="col-12 col-lg-5">
                <div class="content_galerias mb-5 mb-lg-0 text-center">

                    <ul id="Gallery" class="cs-hidden">        
                        <li class="lslide" data-thumb="images/redes.jpg" data-src="images/redes.jpg">                     
                            <img class="img-responsive" src="images/redes.jpg" alt="">
                        </li>
                        <li class="lslide" data-thumb="images/redes.jpg" data-src="images/redes.jpg">
                            <img class="img-responsive" src="images/redes.jpg" alt="">
                        </li>
                        <li class="lslide" data-thumb="images/redes.jpg" data-src="images/redes.jpg">
                            <img class="img-responsive" src="images/redes.jpg" alt="">
                        </li>  
                        <li class="lslide" data-thumb="images/redes.jpg" data-src="images/redes.jpg">
                            <img class="img-responsive" src="images/redes.jpg" alt="">
                        </li>
                        <li class="lslide" data-thumb="images/redes.jpg" data-src="images/redes.jpg">
                            <img class="img-responsive" src="images/redes.jpg" alt="">
                        </li>                     
                    </ul>
                </div>
            </div>
            <div class="col-12 col-lg-7">
                <div class="font-32 text-uppercase negro bar700 pb-3 text-left">hidrocool g30</div>
                <ul class="nav nav-pills mx-0 mb-4" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">descripción</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">propiedades</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">presentación</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="pills-instru-tab" data-toggle="pill" href="#pills-instru" role="tab" aria-controls="pills-instru" aria-selected="false">instrucciones</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-almacenamiento-tab" data-toggle="pill" href="#pills-almacenamiento" role="tab" aria-controls="pills-almacenamiento" aria-selected="false">almacenaje</a>
                    </li>
                </ul>
                <div class="tab-content pb-4" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="gris bar400 font-16">
                            Es un líquido diseñado como agente para el control de Es un líquido diseñado como agente para el control de emisiones de óxidos de nitrógeno provenientes de los gases de combustión de los motores Diesel (vehículos de transporte de carga , barcos, motores estacionarios,Maquinaria agrícola, maquinaria amarilla) con tecnología SCR (Selective Catalytic Reduction) EURO IV-V. HIDROBLUE 1 ® es una solución acuosa de urea sintética grado automotriz al 32% (AUS32).
                        </div>
                        
                    </div>
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <div class="gris bar400 font-16">
                            Es un líquido diseñado como agente para el control de Es un líquido diseñado como agente para el control de emisiones de óxidos de nitrógeno provenientes de los gases de combustión de los motores Diesel (vehículos de transporte de carga , barcos, motores estacionarios,Maquinaria agrícola, maquinaria amarilla) con tecnología SCR (Selective Catalytic Reduction) EURO IV-V. HIDROBLUE 1 ® es una solución acuosa de urea sintética grado automotriz al 32% (AUS32).
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                        <div class="gris bar400 font-16">
                            Es un líquido diseñado como agente para el control de Es un líquido diseñado como agente para el control de emisiones de óxidos de nitrógeno provenientes de los gases de combustión de los motores Diesel (vehículos de transporte de carga , barcos, motores estacionarios,Maquinaria agrícola, maquinaria amarilla) con tecnología SCR (Selective Catalytic Reduction) EURO IV-V. HIDROBLUE 1 ® es una solución acuosa de urea sintética grado automotriz al 32% (AUS32).
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-instru" role="tabpanel" aria-labelledby="pills-instru-tab">
                        <div class="gris bar400 font-16">
                            Es un líquido diseñado como agente para el control de Es un líquido diseñado como agente para el control de emisiones de óxidos de nitrógeno provenientes de los gases de combustión de los motores Diesel (vehículos de transporte de carga , barcos, motores estacionarios,Maquinaria agrícola, maquinaria amarilla) con tecnología SCR (Selective Catalytic Reduction) EURO IV-V. HIDROBLUE 1 ® es una solución acuosa de urea sintética grado automotriz al 32% (AUS32).
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pills-almacenamiento" role="tabpanel" aria-labelledby="pills-almacenamiento-tab">
                        <div class="gris bar400 font-16">
                            Es un líquido diseñado como agente para el control de Es un líquido diseñado como agente para el control de emisiones de óxidos de nitrógeno provenientes de los gases de combustión de los motores Diesel (vehículos de transporte de carga , barcos, motores estacionarios,Maquinaria agrícola, maquinaria amarilla) con tecnología SCR (Selective Catalytic Reduction) EURO IV-V. HIDROBLUE 1 ® es una solución acuosa de urea sintética grado automotriz al 32% (AUS32).
                        </div>
                    </div>
                </div>
                <div class="text-left mt-2">
                    <a class="btn btn-primary border-50 mr-sm-4 mb-3 mb-sm-0" href="#" role="button">Descargar catálogo</a>
                    <a class="btn btn-outline-primary  border-50" href="#" role="button">Quiero ser distribuidor</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="content_nosoForm d-flex  align-items-center">
    <div class="container py-5">
        <div class="row">
            <div class="col-12 col-sm-7 col-md-6 col-lg-5">
                <div class="font-32 bebas700 mb-4 text-uppercase blanco">contáctanos</div>
                <form>
                    <div class="form-group ">
                        <label class="form-label" for="input1">Nombre*</label>
                        <input type="text" id="input1" class="form-control"  />
                    </div>
                    <div class="form-row ">
                        <div class="form-group col-12 col-md-6">
                            <div class="form-group mb-md-1 ">
                                <label class="form-label" for="input2">Teléfono*</label>
                                <input type="text" id="input2" class="form-control"  />
                            </div>
                        </div>
                        <div class="form-group col-12 col-md-6">                         
                            <div class="form-group mb-md-1">
                                <label class="form-label" for="input3">Email*</label>
                                <input type="text" id="input3" class="form-control"  />
                            </div>
                        </div>
                    </div>                    
                    <div class="form-group ">
                        <label class="form-label" for="input6">Mensaje**</label>
                        <textarea name="" id="input6" class="form-control"></textarea>                            
                    </div>                    
                    <div class="form-group">
                        <div class="form-check text-left">
                        <input class="form-check-input" type="checkbox" id="gridCheck">
                            <label class="form-check-label" for="gridCheck">
                            <a href="terminos-condiciones.php" class="blanco" target="_blank">  Acepto términos y condiciones</a>
                            </label>
                        </div>
                    </div>
                    <button type="button" class="btn btn-primary  ">ENVIAR </button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="contnte_produRela py-5">
    <div class="container">
        <div class="font-32 text-uppercase negro bar700 pb-4 text-center">productos relacionados</div>

        <div class="owl-carousel owl-theme owl-productos">
            <div class="item">
                <div class="tab-main-img">
					<img src="images/productos_relacionados.jpg" alt="" width="100%">
					<div class="bg-tab-main"></div>						
					<div class="sub-main-tab">
						<div class="title-main-tab">
                            HidroCool G40
						</div>
						<a href="#" class="link-tab-main">ver más</a>
					</div>
				</div>
            </div>
            <div class="item">
                <div class="tab-main-img">
					<img src="images/productos_relacionados.jpg" alt="" width="100%">
					<div class="bg-tab-main"></div>						
					<div class="sub-main-tab">
						<div class="title-main-tab">
                            HidroCool G40
						</div>
						<a href="#" class="link-tab-main">ver más</a>
					</div>
				</div>
            </div>
            <div class="item">
                <div class="tab-main-img">
					<img src="images/productos_relacionados.jpg" alt="" width="100%">
					<div class="bg-tab-main"></div>						
					<div class="sub-main-tab">
						<div class="title-main-tab">
                            HidroCool G40
						</div>
						<a href="#" class="link-tab-main">ver más</a>
					</div>
				</div>
            </div>
            <div class="item">
                <div class="tab-main-img">
					<img src="images/productos_relacionados.jpg" alt="" width="100%">
					<div class="bg-tab-main"></div>						
					<div class="sub-main-tab">
						<div class="title-main-tab">
                            HidroCool G40
						</div>
						<a href="#" class="link-tab-main">ver más</a>
					</div>
				</div>
            </div>
        </div>
    </div>
</div>




</section>


<?php include ('footer.php') ?>