﻿<?php include ('header.php') ?>


<section id="main" class="main ">

<div class="wrrape-banner" style="background-image: url(images/contacto.jpg)">
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-end">
            <div class="col-12 col-lg-6 text-left">
                <div class="blanco bebas700 font-36 text-uppercase">CONTACTO</div>
            </div>
            <div class="col-12 col-lg-6 d-none d-lg-block">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb p-0 mb-0 bg-transparent justify-content-end">
                        <li class="breadcrumb-item"><a href="#" class="blanco bar200 font-18">Home</a></li>
                        <li class="breadcrumb-item font-18 blanco bar200 active" aria-current="page">Contacto</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="content_contacto  py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-5 order-2 order-md-1">
                <div class="content-form">
                    <form class=""  method="post">                        
                        <div class="form-group ">
                            <label class="form-label" for="input1">Nombre*</label>
                            <input type="text" id="input1" class="form-control"  />
                        </div>
                        <div class="form-group ">
                            <label class="form-label" for="input2">Teléfono*</label>
                            <input type="text" id="input2" class="form-control"  />
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="input3">Email*</label>
                            <input type="text" id="input3" class="form-control"  />
                        </div>
                        <div class="form-group ">
                            <label class="form-label" for="input4">País*</label>
                            <input type="text" id="input4" class="form-control"  />
                        </div>
                        <div class="form-group ">
                            <label class="form-label" for="input5">Asunto*</label>
                            <input type="text" id="input5" class="form-control"  />
                        </div>
                        <div class="form-group ">
                            <label class="form-label" for="input6">Mensaje**</label>
                            <textarea name="" id="input6" class="form-control"></textarea>                            
                        </div>
                        
                        <div class="form-group">
                            <div class="form-check text-center">
                            <input class="form-check-input" type="checkbox" id="gridCheck">
                               <label class="form-check-label" for="gridCheck">
                                <a href="terminos-condiciones.php" class="negro" target="_blank">  Acepto términos y condiciones</a>
                                </label>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="button" class="btn btn-outline-primary  ">ENVIAR </button>
                        </div>
                    </form>
                </div>
            </div> 
            <div class="col-12 col-md-6 col-lg-7 mb-5 mb-lg-0  order-1 order-md-2">
                <div class="content_info mb-4">
                    <aside>
                        <div class="media w-100 mb-4">
                            <img src="images/icon01.png" class="mr-3" alt="...">
                            <div class="media-body  font-18 gris bar400">
                                <h5 class="mt-0 bar700 mb-0 d-block font-18">Dirección</h5>                                                                
                                Parque Industrial San Miguel Bodega 1a <br>
                                km 1.5 via siberia-cota Entrada potrero chico <br>
                                Cota, cundinamarca, Colombia.
                            </div>
                        </div>
                        <div class="media w-100 mb-4">
                            <img src="images/icon02.png" class="mr-3" alt="...">
                            <div class="media-body font-18 gris bar400">
                                <h5 class="mt-0 bar700 mb-0 d-block font-18">Email</h5>                                                                                    
                                soporteyventas@hidrotecnik.com <br>
                                ventas1@hidrotecnik.com
                            </div>
                        </div>
                        <div class="media w-100 mb-4 mb-lg-0">
                            <img src="images/icon03.png" class="mr-3" alt="...">
                            <div class="media-body">
                                <div class="conte font-18 gris bar400 mb-3">
                                    <h5 class="mt-0 bar700 mb-0 d-block font-18">Línea de atenció al cliente</h5>    
                                    (+571) 8985017 • (+571) 8985027 
                                </div>   
                                <div class="conte font-18 gris bar400">
                                    <h5 class="mt-0 bar700 mb-0 d-block font-18">Celular ventas</h5> 
                                    (+57) 321 201 48 37 • (+57) 317 278 80 75
                                </div>                      
                            </div>
                        </div>
                    </aside>
                </div>
                <div class="map-canvas" id="map-canvas"></div>
            </div>
        </div>
       
    </div>
</div>




</section>


<?php include ('footer.php') ?>