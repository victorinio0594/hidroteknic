﻿<?php include ('header.php') ?>


<section id="main" class="main ">

<div class="wrrape-banner" style="background-image: url(images/contacto.jpg)">
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-end">
            <div class="col-12 col-lg-6 text-left">
                <div class="blanco bebas700 font-36 text-uppercase">PQRS</div>
            </div>
            <div class="col-12 col-lg-6 d-none d-lg-block">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb p-0 mb-0 bg-transparent justify-content-end">
                        <li class="breadcrumb-item"><a href="#" class="blanco bar200 font-18">Home</a></li>
                        <li class="breadcrumb-item bar200 font-18 blanco active" aria-current="page">Hidrotecnik</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>


<div class="content_contacto  py-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 col-lg-5 order-2 order-md-1">
                <div class="content-form">
                    <form class=""  method="post">                        
                        <div class="form-group ">
                            <label class="form-label" for="input1">Nombre*</label>
                            <input type="text" id="input1" class="form-control"  />
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="inputSelect">Tipo de identificación</label>
                            <select class="form-control custom-select" id="inputSelect"  >
                                <option> </option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>                      
                        <div class="form-group">
                            <label class="form-label" for="inputIdent">Número de identificación*</label>
                            <input type="text" id="inputIdent" class="form-control"  />
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="input3">Empresa*</label>
                            <input type="text" id="input3" class="form-control"  />
                        </div>
                        <div class="form-group ">
                            <label class="form-label" for="input2">Teléfono*</label>
                            <input type="text" id="input2" class="form-control"  />
                        </div>
                        <div class="form-group ">
                            <label class="form-label" for="input4">Email*</label>
                            <input type="text" id="input4" class="form-control"  />
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="inputSelect">Tipo de requerimiento</label>
                            <select class="form-control custom-select" id="inputSelect"  >
                                <option> </option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>    
                        <div class="form-group ">
                            <label class="form-label" for="input5">Asunto*</label>
                            <input type="text" id="input5" class="form-control"  />
                        </div>
                        <div class="form-group ">
                            <label class="form-label" for="input6">Mensaje**</label>
                            <textarea name="" id="input6" class="form-control"></textarea>                            
                        </div>
                        
                        <div class="form-group">
                            <div class="form-check text-center">
                            <input class="form-check-input" type="checkbox" id="gridCheck">
                               <label class="form-check-label" for="gridCheck">
                                <a href="terminos-condiciones.php" class="negro" target="_blank">  Acepto términos y condiciones</a>
                                </label>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="button" class="btn btn-outline-primary  ">ENVIAR </button>
                        </div>
                    </form>
                </div>
            </div> 
            <div class="col-12 col-md-6 col-lg-7 mb-5 mb-lg-0  order-1 order-md-2">
                <div class="gris mb-3 font-16 bar400"> 
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Repellat, quidem labore! Officiis a consectetur, nisi iure, aliquid cumque adipisci officia eaque at in tempora qui. Ad sed blanditiis molestias voluptas.
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Iure facere aspernatur dolore illo dignissimos quia, odio laboriosam! Pariatur eligendi, velit provident quas omnis id itaque quia repudiandae non minima natus.
                </div>

                <div class="style-2">
                    <img src="images/distribuidores.jpg" class="w-100" alt="">
                </div>
               


            </div>
        </div>
       
    </div>
</div>


<!-- 

<div class="content_showus  py-5 position-relative">
    <div class="content_pqrs h-100">
        <div class="container ">
            <div class="row">
                <div class="col-12 col-lg-7 mb-5 mb-lg-0">
                    <div class="accordion mt-3" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne">                            
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    hidrotecnik
                                </button>                            
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                Hidrotecnik, es el primer productor certificado en Colombia, para la venta de Urea Automotriz, para el consumo de las flotas, sistemas de transporte masivo y motores diesel con motores Euro 4, 5 y 6.
                            </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">                        
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    ventas
                                </button>                     
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">                       
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    proveedores
                                </button>                        
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">                       
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    proveedores
                                </button>                        
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">                       
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    proveedores
                                </button>                        
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">                       
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    proveedores
                                </button>                        
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">                       
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    proveedores
                                </button>                        
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>

 -->

</section>


<?php include ('footer.php') ?>